/**
 * 后端启动
 */
package org.beykery.bakka.test;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import java.io.File;
import org.beykery.bakka.Bootstrap;

/**
 *
 * @author beykery
 */
public class TestBootstrap
{

  public static void main(String[] args) throws InterruptedException, Exception
  {

//    Bootstrap bs = Bootstrap.newInstance(new File("./bakka.conf"));
//    bs.start();
//    Thread.sleep(2);    
//    ActorSelection as = bs.getActorSystem().actorSelection("/user/Fronted");
//    as.tell(new HI("hi, I'm admin!"),ActorRef.noSender());
    
    
    Bootstrap bs2 = Bootstrap.newInstance(new File("./bakka_backend.conf"));
    bs2.start();
    Thread.sleep(10*1000);
//    Thread.sleep(2);
//    ActorSelection as2 = bs2.getActorSystem().actorSelection("/user/Backend");
//    as2.tell(new HI("hi, I'm backend!"),ActorRef.noSender());
    
    
    Bootstrap bs1 = Bootstrap.newInstance(new File("./bakka_fronted.conf"));
    bs1.start();
    Thread.sleep(15*1000);
    ActorSelection as1 = bs1.getActorSystem().actorSelection("/user/Fronted");
    as1.tell(new HI("hi"), ActorRef.noSender());

//    HI hi = (HI) bs.locatActorSend(Admin.class, new HI("i am hi"));
//    System.out.println("hi="+hi);
//    as.tell(new Tree("root"), ActorRef.noSender());
//    Thread.sleep(30000); 
//    Tree tree = (Tree) bs.locatActorSend(Admin.class, new Tree("root"));
  }
}
